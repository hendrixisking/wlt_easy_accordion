# Interview task given by Williams Lea

[![Williams Lea](https://www.williamslea.com/wp-content/themes/wlt-website/assets/src/images/wl-logo.svg "Williams Lea")](https://www.williamslea.com/ "Williams Lea")

### What is this repository for?

This repository contains more solutions of the accordion task.

### How do I get set up?

All files are static HTML, no need to set up or run a dev environment to view the pages. Simple open the local files in your browser.

### Remarks

1. For better UX a nice colour scheme is used, colours are from a design palette:
   https://coolors.co/264653-2a9d8f-e9c46a-f4a261-e76f51
   https://coolors.co/f8f9fa-e9ecef-dee2e6-ced4da-adb5bd-6c757d-495057-343a40-212529
2. Markup been checked against W3C standards and passed.
3. As on the previous stage of interview we talked about Object Oriented CSS I followed that practice in this solution.
4. As this file is simple HTML file without and SASS/LESS pre-processors to use I just used CSS vars to store colour codes. It's cross browser approach (https://caniuse.com/?search=var).
5. For the purpose of decrease filesize instead of using SVG (or even an old transparent PNG...) for arrow element it was crated by pseudo constructors in CSS. Clean, simple.
6. As we talked earlier WL doesn't support IE11, that is the reason why I used template literals in JavaScript. As we know it is not supported by IE11 but the rest of the modern browsers.
7. Solution was checked in Chrome Version 89.0.4389.90 (Official Build) (x86_64)
8. Accessibility labels make the DOM harder to read so I added this functionality only to solution-v4. Each solution is a little bit more than the previous one.

### Demo

1. Original task file is [here](http://www.domainforssl.hu/interviewtask/williamlea/WL-Interview-challenge.html "here")
2. [solution-v1](http://www.domainforssl.hu/interviewtask/williamlea/solution-v1.html "solution-v1"): the very first solution using simple show/hide content when opening an accordion item
3. [solution-v2](http://www.domainforssl.hu/interviewtask/williamlea/solution-v2.html "solution-v2"): same as v1 but using animation instead of show/hide content. Animated property is max-height.
4. [solution-v3](http://www.domainforssl.hu/interviewtask/williamlea/solution-v3.html "solution-v3"): same as v1 but using JavaScript fuctions to show/hide content of an accordion item. At page load event a script called "initAccordionItems" get the height of each content panel, saves it into data attribute and set inline style to 0 (hide them). When clicking on an item the script will set the height of the content stored in data attribute. If content is long the data attribute should be updated when window is resized.
5. [solution-v4](http://www.domainforssl.hu/interviewtask/williamlea/solution-v4.html "solution-v4"): same as v1 but **with accessibility**!

### Main technologies used in this project

- HTML5 (W3C)
- CSS3
- JavaScript
